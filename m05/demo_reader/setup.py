import setuptools

setuptools.setup(
    name="demo_reader",
    version="0.0.0",
    description="Tools for reading various file formats",
    packages=setuptools.find_packages('src'),
    package_dir={'': 'src'},
)

