======================================================
Core Python: Organizing Larger Programs - Example Code
======================================================

This contains the example code we use in the Pluralsight course `Core Python: Organizing Larger Programs
<https://app.pluralsight.com/library/courses/core-python-organizing-larger-programs/>`_. Work is underway to re-build
this course, after which this material will be available as part of the standard "exercises" file in the course.