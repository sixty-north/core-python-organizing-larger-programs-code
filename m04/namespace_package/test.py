import subprocess
import sys

from reader.multireader import MultiReader

test_data = {
    'bzipped': ('test.bz2', "compressed with bz2"),
    'gzipped': ('test.gz', 'compressed with gz'),
}

for module, (filename, contents) in test_data.items():
    command = 'python -m reader.compressed.{} {} {}'.format(module, filename, contents)
    subprocess.run(command.split())
    r = MultiReader(filename)
    assert r.read() == contents
    r.close()

r = MultiReader('path1/reader/util/__init__.py')
assert r.read() == ''
r.close()
