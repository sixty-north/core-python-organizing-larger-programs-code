# multi-reader-program-program/__main__.py

import sys

import demo_reader
from demo_reader.multireader import MultiReader

print(demo_reader.__file__)

filename = sys.argv[1]
r = MultiReader(filename)
print(r.read())
r.close()